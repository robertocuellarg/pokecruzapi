from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker,relationship
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, ForeignKey, UniqueConstraint, func, DateTime
import time

DB_ENGINE = 'mysql://iluretar:masbienloquita@172.30.46.113:3306/pokecruz'
#DB_ENGINE = 'mysql://iluretar:masbienloquita@127.0.0.1:3306/pokecruz'

def get_engine():
    return create_engine(DB_ENGINE)

Base = declarative_base()


class Account(Base):
    __tablename__ = 'accounts'

    id = Column(Integer,primary_key=True)
    user_email = Column(String(64))
    user_device_id = Column(String(64))
    user_code = Column(String(16))


class Sighting(Base):
    __tablename__ = 'sightings'

    id = Column(Integer, primary_key=True)
    pokemon_id = Column(Integer)
    spawn_id = Column(String(32))
    expire_timestamp = Column(Integer, index=True)
    encounter_id = Column(String(32))
    normalized_timestamp = Column(Integer)
    lat = Column(String(20), index=True)
    lon = Column(String(20), index=True)

class Fort(Base):
    __tablename__ = 'forts'

    id = Column(Integer, primary_key=True)
    external_id = Column(String(64), unique=True)
    lat = Column(String(20), index=True)
    lon = Column(String(20), index=True)

    sightings = relationship(
        'FortSighting',
        backref='fort',
        order_by='FortSighting.last_modified'
    )


class FortSighting(Base):
    __tablename__ = 'fort_sightings'

    id = Column(Integer, primary_key=True)
    fort_id = Column(Integer, ForeignKey('forts.id'))
    last_modified = Column(Integer)
    team = Column(Integer)
    prestige = Column(Integer)
    guard_pokemon_id = Column(Integer)

    __table_args__ = (
        UniqueConstraint(
            'fort_id',
            'last_modified',
            name='fort_id_last_modified_unique'
        ),
    )

Session = sessionmaker(bind=get_engine())

def get_account(session, user_email):
    return session.query(Account) \
        .filter(Account.user_email == user_email) \
        .first()

def validate_user(session, user_device_id):
    return session.query(Account) \
        .filter(Account.user_device_id == user_device_id) \
        .first()

def get_sightings(session):
    return session.query(Sighting) \
        .filter(Sighting.expire_timestamp > time.time()) \
        .all()


def get_forts(session):
    query = session.execute('''
        SELECT
            fs.fort_id,
            fs.id,
            fs.team,
            fs.prestige,
            fs.guard_pokemon_id,
            fs.last_modified,
            f.lat,
            f.lon
        FROM fort_sightings fs
        INNER JOIN (SELECT MAX(fs.last_modified) AS last_modified, fs.fort_id FROM fort_sightings fs GROUP BY fort_id) lfs ON lfs.fort_id = fs.fort_id AND lfs.last_modified = fs.last_modified
        INNER JOIN forts f ON f.id=fs.fort_id
        ''')
    return query.fetchall()

from flask import Flask, request, Response, render_template, send_file
from functools import wraps
from names import POKEMON_NAMES
import json
import db


application = Flask(__name__, template_folder='templates')

def check_auth(key):
    session = db.Session()
    account = db.validate_user(session,key)
    session.close()
    if account:
        return key == account.user_device_id
    else:
        return False

def authenticate():
    return Response(
    'Could not verify your access level for that URL.\n'
    'You have to login with proper credentials', 401)

def requires_auth(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        api_key = None
        if request.headers.get('pokecruz'):
            api_key = request.headers['pokecruz']
        if not api_key or not check_auth(api_key):
            return authenticate()
        return f(*args, **kwargs)
    return decorated

@application.route("/")
def hello():
    return render_template('index.html')

@application.route("/<file_name>")
def getFile(file_name):
    return send_file(file_name, as_attachment=True)

@application.route('/account')
def user_data():
    user_email = request.args.get('user_email')
    user = []
    session = db.Session()
    account = db.get_account(session,user_email)
    session.close()
    if account :
        user.append({
            'id': account.id,
            'user_email': account.user_email,
            'user_device_id': account.user_device_id,
            'user_code': account.user_code,
        })
    return json.dumps(user)

@application.route('/pokemon')
@requires_auth
def get_pokemons():
    markers = []
    session = db.Session()
    pokemons = db.get_sightings(session)
    session.close()
    for pokemon in pokemons:
        markers.append({
            'id': 'pokemon-{}'.format(pokemon.id),
            'type': 'pokemon',
            'name': POKEMON_NAMES[pokemon.pokemon_id],
            'pokemon_id': pokemon.pokemon_id,
            'lat': pokemon.lat,
            'lon': pokemon.lon,
            'expires_at': pokemon.expire_timestamp,
        })
    return json.dumps(markers)

@application.route('/fort')
@requires_auth
def get_forts():
    markers = []
    session = db.Session()
    forts = db.get_forts(session)
    session.close()
    for fort in forts:
        if fort['guard_pokemon_id']:
            pokemon_name = POKEMON_NAMES[fort['guard_pokemon_id']]
        else:
            pokemon_name = 'Empty'
        markers.append({
            'id': 'fort-{}'.format(fort['fort_id']),
            'sighting_id': fort['id'],
            'type': 'fort',
            'prestige': fort['prestige'],
            'pokemon_id': fort['guard_pokemon_id'],
            'pokemon_name': pokemon_name,
            'team': fort['team'],
            'lat': fort['lat'],
            'lon': fort['lon'],
        })
    return json.dumps(markers)

if __name__ == "__main__":
    application.run(debug=True)
